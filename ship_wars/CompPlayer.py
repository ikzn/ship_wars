from player import Player
from ComputerPlayer import ComputerPlayer

num_cells = 10
own_player = Player(num_cells)
comp_player = ComputerPlayer(num_cells)
own_player.enemy = comp_player

comp_player.install_all_ships()

own_player.print_ships()
comp_player.print_ships()
