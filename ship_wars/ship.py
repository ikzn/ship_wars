from cellstatus import Status
from time import sleep
from PyQt5 import QtWidgets, uic, QtCore, QtGui, Qt

class Ship(object):
    def __init__(self, occupied_cells,battlefield):
        self.battlefield = battlefield
        self.occupied_cells = occupied_cells
        for cell in occupied_cells:
            cell.ship = self
            cell.status = Status.BUSY

    def get_cells_around(self):
        around_cells = []
        for ships_cell in self.occupied_cells:
            for i in range(ships_cell.x - 1, ships_cell.x + 2):
                for j in range(ships_cell.y - 1, ships_cell.y + 2):
                    if ((i != ships_cell.x or j != ships_cell.y) and i >= 0 and i < self.battlefield.n and j >= 0 and j < self.battlefield.n):
                        around_cells.append(self.battlefield[i][j])
        return around_cells

    def get_dim(self):
        return len(self.occupied_cells)

    def is_alive(self):
        for cell in self.occupied_cells:
            if (cell.status != Status.HIT):
                return True
        return False

    def die(self):
        try:
            for cell in self.occupied_cells:
                cell.widget.kill()
            a = self.get_cells_around()
            for cell_in_ship in a:
                # print(cell_in_ship.x)
                # print(cell_in_ship.y)
                if(cell_in_ship.status != Status.HIT):
                    around_cell_widget = cell_in_ship.widget
                    cell_in_ship.status = Status.MISS
                    around_cell_widget.setIcon(Qt.QIcon('./ui/res/miss_cell.jpg'))
                    around_cell_widget.setIconSize(Qt.QSize(50, 50))
                    around_cell_widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность
        except NameError:
            pass
            # print("error in ship.die: ", NameError)

    def print_cells(self):
        for cell in self.occupied_cells:
            pass
            # print(cell.x, cell.y, cell.status, end=', ')
        # print()
