from cell import Cell
class BattleField:
    cells = []

    def __init__(self, N):
        self.n = N
        self.cells = []
        for i in range(0, N):
            row = []
            for j in range(0, N):
                row.append(Cell(i, j))
            self.cells.append(row)

    def __iter__(self):
        return self.cells.__iter__()

    def __getitem__(self, i):
        return self.cells[i]

    def print_all_statuses(self):
        for row in self.cells:
            for cell in row:
                pass
                # print('%d' % cell.is_busy(), end='\t')
            # print()

    def is_cell_with_busy_neighbours(self, x, y):
        for i in range(x - 1, x + 2):
            for j in range(y - 1, y + 2):
                if (
                    (i != x or j != y) and
                    i >= 0 and i < self.n and
                    j >= 0 and j < self.n and
                    self[i][j].is_busy()
                ):
                    return True
        return False