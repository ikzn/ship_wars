from cellstatus import Status

class Cell(object):
    status = Status.FREE

    # помимо статуса, x и y, есть еще и ship и widget

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __len__(self):
        return 1

    def is_busy(self):
        return self.status in [Status.BUSY, Status.HIT]

    def was_fired(self):
        return self.status in [Status.MISS, Status.HIT]