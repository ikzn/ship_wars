import random
from player import Player
from cellstatus import Status
from ship import Ship
from battlefield import BattleField

class ComputerPlayer(Player):
    def __init__(self, size, enemy=None, level = 0):
        self.size = size
        self.battlefield = BattleField(size)
        self.ships = []
        self.enemy = enemy
        self.level = level

    def set_level(self, level):
        self.level = level

    def fire_cell(self):
        try:
            cell = self.get_fire_cell(self.level)
            return cell.widget.try_hit_cell(self)
        except NameError:
            pass
            # print("cell.widget is not defined: ",NameError)

    def get_fire_cell(self,level=1):
        if(level == 0):
            for row in self.enemy.battlefield.cells:
                for cell in row:
                    if(not (cell.was_fired())):
                        return cell

        elif(level == 1):
            flag_for_busy = 1
            while (flag_for_busy):
                x = random.randint(0, self.size - 1)
                y = random.randint(0, self.size - 1)
                if (not self.enemy.battlefield[x][y].was_fired()):
                    flag_for_busy = 0
                    return self.enemy.battlefield[x][y]
                else:
                    should_continue = False
                    for player_ship in self.enemy.ships:
                        if (player_ship.is_alive()):
                            should_continue = True
                    if (not should_continue):
                        break

        elif (level == 2):
            for row in self.enemy.battlefield.cells:
                for cell in row:
                    if (cell.status == Status.HIT):
                        # если корабль ещё жив
                        if (cell.ship.is_alive()):
                            # 4 точки вокруг HIT, (a_p_x - array_point_x)
                            a_p_x = []
                            a_p_y = []
                            status_point = []
                            # 1
                            if ((cell.x + 1) < self.enemy.battlefield.n):
                                a_p_x.append(cell.x + 1)
                                a_p_y.append(cell.y)
                                status_point.append(0)
                                # print("0")
                                # # print(self.enemy.battlefield[a_p_x[0]][a_p_y[0]].status)
                            # 2
                            if ((cell.x - 1) >= 0):
                                a_p_x.append(cell.x - 1)
                                a_p_y.append(cell.y)
                                status_point.append(1)
                                # print("1")
                                # # print(self.enemy.battlefield[a_p_x[1]][a_p_y[1]].status)
                            # 3
                            if ((cell.y + 1) < self.enemy.battlefield.n):
                                a_p_x.append(cell.x)
                                a_p_y.append(cell.y + 1)
                                status_point.append(2)
                                # print("2")
                                # # print(self.enemy.battlefield[a_p_x[2]][a_p_y[2]].status)
                            # 4
                            if ((cell.y - 1) >= 0):
                                a_p_x.append(cell.x)
                                a_p_y.append(cell.y - 1)
                                status_point.append(3)
                                # print("3")
                                # # print(self.enemy.battlefield[a_p_x[3]][a_p_y[3]].status)

                            # fix, если между miss и hit
                            if (((cell.x + 1) < self.enemy.battlefield.n) and (cell.x - 1 >= 0)):
                                if((self.enemy.battlefield[cell.x + 1][cell.y].status == Status.HIT) and (self.enemy.battlefield[cell.x - 1][cell.y].status == Status.MISS)):
                                    # print("int1")
                                    continue
                            if (((cell.y + 1) < self.enemy.battlefield.n) and (cell.y - 1 >= 0)):
                                if((self.enemy.battlefield[cell.x][cell.y + 1].status == Status.HIT) and (self.enemy.battlefield[cell.x][cell.y - 1].status == Status.MISS)):
                                    # print("int2")
                                    continue


                            # выписываем варианты
                            variants = []
                            for i in range(0, len(a_p_x)):
                                # print(i)
                                # print(not self.enemy.battlefield[a_p_x[i]][a_p_y[i]].was_fired())
                                # print(a_p_x[i] >= 0 and a_p_x[i] < self.enemy.battlefield.n and a_p_y[i] >= 0 and a_p_y[i] < self.enemy.battlefield.n)
                                if ((not self.enemy.battlefield[a_p_x[i]][a_p_y[i]].was_fired()) and a_p_x[i] >= 0 and a_p_x[i] < self.enemy.battlefield.n and a_p_y[i] >= 0 and a_p_y[i] < self.enemy.battlefield.n):
                                    variants.append(i)

                            # если есть предыстория
                            for i in variants:
                                if (status_point[i] == 0 and (cell.x - 2 >= 0)):
                                    if (self.enemy.battlefield[cell.x - 2][cell.y].status == Status.HIT):
                                        return self.enemy.battlefield[a_p_x[i]][a_p_y[i]]
                                if (status_point[i] == 1 and (cell.x + 2 < self.enemy.battlefield.n)):
                                    if (self.enemy.battlefield[cell.x + 2][cell.y].status == Status.HIT):
                                        return self.enemy.battlefield[a_p_x[i]][a_p_y[i]]
                                if (status_point[i] == 2 and (cell.y - 2 >= 0)):
                                    if (self.enemy.battlefield[cell.x][cell.y - 2].status == Status.HIT):
                                        return self.enemy.battlefield[a_p_x[i]][a_p_y[i]]
                                if (status_point[i] == 3 and (cell.y + 2 < self.enemy.battlefield.n)):
                                    if (self.enemy.battlefield[cell.x][cell.y + 2].status == Status.HIT):
                                        return self.enemy.battlefield[a_p_x[i]][a_p_y[i]]

                            # если нет предыстории
                            if (len(variants) > 0):
                                random_point = random.randint(0, len(variants) - 1)
                                # print("random")
                                # print(a_p_x[random_point])
                                # print(a_p_y[random_point])
                                return self.enemy.battlefield[a_p_x[variants[random_point]]][a_p_y[variants[random_point]]]

            flag_for_busy = 1
            while (flag_for_busy):
                x = random.randint(0, self.size - 1)
                y = random.randint(0, self.size - 1)
                if (not self.enemy.battlefield[x][y].was_fired()):
                    flag_for_busy = 0
                    return self.enemy.battlefield[x][y]
                else:
                    should_continue = False
                    for player_ship in self.enemy.ships:
                        if(player_ship.is_alive()):
                            should_continue = True
                    if(not should_continue):
                        break


