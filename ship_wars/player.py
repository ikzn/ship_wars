import random
from ship import Ship
from battlefield import BattleField

class Player:
    def __init__(self, size, enemy=None):
        self.size = size
        self.battlefield = BattleField(size)
        self.ships = []
        self.enemy = enemy

    def is_defeated(self):
        for ship in self.ships:
            if (ship.is_alive()):
                return False
        return True


    def print_ships(self):
        for ship in self.ships:
            ship.print_cells()
        # print()


    #метод для расстановки кораблей
    def install_all_ships(self):

        error = 0

        #четырехпалубник
        flag = self.install_ship(4)
        while(flag == 1):
            flag = self.install_ship(4)
            error = error + 1

        #2 трехпалубника
        for i in range(2):
            flag = self.install_ship(3)
            while (flag == 1):
                flag = self.install_ship(3)
                error = error + 1

        #3 двухпалубника
        for i in range(3):
            flag = self.install_ship(2)
            while (flag == 1):
                flag = self.install_ship(2)
                error = error + 1

        #4 однопалубника
        for i in range(4):
            flag = self.install_ship(1)
            while (flag == 1):
                flag = self.install_ship(1)
                error = error + 1

        # print("Количество ошибок ",error)


    def install_ship(self,ship_size):
        # получение места установки
        place_i = random.randint(0, self.size - 1)
        place_j = random.randint(0, self.size - 1)

        # проверяем наличие короблей поблизости
        while(self.battlefield.is_cell_with_busy_neighbours(place_i, place_j)):
            place_i = random.randint(0, self.size - 1)
            place_j = random.randint(0, self.size - 1)


        # print(self.battlefield.is_cell_with_busy_neighbours(place_i, place_j), place_i, place_j)

        # выбираем направление
        direction_x = random.randint(0, 1)
        ship_occupied_cells = []
        ship_occupied_cells.append(self.battlefield[place_i][place_j])
        new_place_i = place_i
        new_place_j = place_j
        # print("direction ", direction_x)

        #флаг отвечает за только одну смену направления
        flag = 0

        if (direction_x):
            direction = 1
            while (ship_size - 1):
                new_place_i = new_place_i + direction
                # print("direction_x ", new_place_i, new_place_j,
                #      self.battlefield.is_cell_with_busy_neighbours(new_place_i, new_place_j), ship_size)
                if ((not self.battlefield.is_cell_with_busy_neighbours(new_place_i, new_place_j)) and (
                        new_place_i < self.size and new_place_i > 0)):
                    ship_occupied_cells.append(self.battlefield[new_place_i][new_place_j])
                    ship_size = ship_size - 1
                else:
                    if(flag == 1):
                        return 1
                    flag = 1
                    direction = -1
                    new_place_i = place_i
        else:
            direction = 1
            while (ship_size - 1):
                new_place_j = new_place_j + direction
                # print("direction_y ", new_place_i, new_place_j,
                #      self.battlefield.is_cell_with_busy_neighbours(new_place_i, new_place_j), ship_size)
                if ((not self.battlefield.is_cell_with_busy_neighbours(new_place_i, new_place_j)) and (
                        new_place_j < self.size and new_place_j > 0)):
                    ship_occupied_cells.append(self.battlefield[new_place_i][new_place_j])
                    ship_size = ship_size - 1
                else:
                    if(flag == 1):
                        return 1
                    flag = 1
                    direction = -1
                    new_place_j = place_j

        # print(ship_occupied_cells)
        new_ship = Ship(ship_occupied_cells,self.battlefield)
        self.ships.append(new_ship)
