# import PyQt5
from PyQt5 import QtWidgets, uic, QtCore, QtGui
import sys
from PlayerFrame import PlayerFrame

player_names = []

class Help(QtWidgets.QDialog, QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi('./ui/help.ui', self)
        self.btn_close.clicked.connect(self.close)
        self.btn_donate.clicked.connect(self.on_donate_clicked)

    def on_donate_clicked(self):
        QtWidgets.QMessageBox.information(self, 'Oops!', 'Just kiddin\' :) \nGive me your cash!')


class ScoreBoard(QtWidgets.QDialog, QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi('./ui/score_board.ui', self)
        self.table_model = QtGui.QStandardItemModel(0, 2, self)
        #self.table_model.setHorizontalHeaderItem(0, QtGui.QStandardItem("#"))
        self.table_model.setHorizontalHeaderItem(0, QtGui.QStandardItem("Player name"))
        self.table_model.setHorizontalHeaderItem(1, QtGui.QStandardItem("Number of moves"))

        self.highScoreTbl.setModel(self.table_model)

        # CONNECTIONS
        self.btn_gotit.clicked.connect(self.close)  # self.close)
        #self.items = []
        self.fill()

    def fill(self):
        global player_names

        tempFile = open('records.rec', 'a+')
        tempFile.close()
        with open('records.rec', 'r+', encoding='utf-8') as records:
            for line in records:
                lst = line.split(';')
                player_name = str(lst[0])
                player_record = str(lst[1])
                item0 = QtGui.QStandardItem(player_name)
                item1 = QtGui.QStandardItem(player_record)
                self.table_model.appendRow([item0, item1])
                player_names.append(player_name)

        #for i in range(0, 3):
            #self.items.append([random.randint(1,3) for i in range(0, 3)])
            #item0 = QtGui.QStandardItem(str(self.items[i][0][0]))
            #item1 = QtGui.QStandardItem(str(self.items[i][0][1]))
            #item2 = QtGui.QStandardItem(str(self.items[i][0][2]))
            #self.table_model.appendRow([item0, item1, item2])


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi('./ui/menu.ui', self)
        # self.setupUi(self)
        self.setStyleSheet("MainWindow { background-image:url(./ui/background_.jpg)}");
        self.resize(500, 750)
        self.setWindowFlags(
            QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.CustomizeWindowHint)
        self.score_board = None
        # self.score_board.setModal(True)
        self.help = Help(self)
        self.player_frame = None

        # CONNECTIONS
        self.btn_exit.clicked.connect(self.close)
        self.btn_high_score.clicked.connect(self.on_scorebrd_open)
        self.btn_help.clicked.connect(self.on_help_open)
        self.btn_play.clicked.connect(self.on_play_clicked)

    def on_play_clicked(self):
        global player_names
        self.player_frame = PlayerFrame(player_names)
        self.player_frame.show()
        # print(self.player_frame)

    def on_scorebrd_open(self):
        # self.hide()
        self.score_board = ScoreBoard(self)
        self.score_board.exec_()
        # uic.loadUi('score_board.ui', self)

    def on_help_open(self):
        self.help.exec_()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    app.exec_()


    #
    # app = QtWidgets.QApplication(sys.argv)
    # window = QtWidgets.QWidget()
    # window.setWindowTitle("ShipWars pre-alpha")
    # window.resize(600, 600)
    #
    # window.show()
    #
    # sys.exit(app.exec_())

    # http://blog.digmag.pw/ru/2016/04/14/%D1%83%D1%80%D0%BE%D0%BA%D0%B8-python-pyqt-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-3-gui-%D0%B2-qtdesigner-2/
    # http://python-3.ru/page/menu-toolbar-pyqt-python
    # http://pyqt.sourceforge.net/Docs/PyQt5/designer.html
    # https://wiki.python.org/moin/JonathanGardnerPyQtTutorial
    # https://www.safaribooksonline.com/blog/2014/01/22/create-basic-gui-using-pyqt/
    # http://pythonforengineers.com/your-first-gui-app-with-python-and-pyqt/
