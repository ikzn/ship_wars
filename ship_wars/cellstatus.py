from enum import Enum
class Status(Enum):
    FREE = 0
    BUSY = 1
    MISS = 2
    HIT  = 3