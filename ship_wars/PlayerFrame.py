
from PyQt5 import QtWidgets, uic, QtCore, QtGui, Qt
import sys
from cellstatus import Status
from time import sleep
from ship import Ship
from enum import Enum
from player import Player
from ComputerPlayer import ComputerPlayer

COMP_DELAY = 0.1
num_cells = 10
player_frame = None
# own_player = Player(num_cells)
# comp_player = ComputerPlayer(num_cells, own_player)
# own_player.enemy = comp_player
# comp_player.install_all_ships()
# num_moves = 0
player_name = 'Player'

class GameStatus(Enum):
    PREPARE = 0
    GAME    = 1
    END     = 2

class FieldProperty(Enum):
    LOCKED = 0
    OPEN   = 1
    ENEMY  = 2
    OWN    = 3

# game_status = GameStatus.PREPARE

class FieldChecker(object):
    def __init__(self):
        self.busy_cells = []
        self.ships = []

    def right(self, field, row, col):
        if col == num_cells - 1:
            return Status.FREE
        return field.itemAtPosition(row, col + 1).widget().get_status()

    def left(self, field, row, col):
        if col == 0:
            return Status.FREE
        return field.itemAtPosition(row, col - 1).widget().get_status()

    def up(self, field, row, col):
        if row == 0:
            return Status.FREE
        return field.itemAtPosition(row - 1, col).widget().get_status()

    def down(self, field, row, col):
        if row == num_cells - 1:
            return Status.FREE
        return field.itemAtPosition(row + 1, col).widget().get_status()

    def check_busy(self, row, col):
        if [row, col] in self.busy_cells:
            return True
        return False

    def check_near_hrz(self, field, row, icol, fcol):
        start_row = max(row - 1, 0)
        final_row = min(row + 1, num_cells - 1)
        start_col = max(icol - 1, 0)
        final_col = min(fcol + 1, num_cells - 1)

        for idx_row in range(start_row, final_row + 1):
            for idx_col in range(start_col, final_col + 1):
                if idx_row == row and idx_col in range(icol, fcol + 1):
                    continue
                else:
                    if field.itemAtPosition(idx_row, idx_col).widget().get_status() == Status.BUSY:
                        raise RuntimeError("Incorrect set of ships")

    def check_near_vrt(self, field, col, irow, frow):
        start_row = max(irow - 1, 0)
        final_row = min(frow + 1, num_cells - 1)
        start_col = max(col - 1, 0)
        final_col = min(col + 1, num_cells - 1)

        for idx_row in range(start_row, final_row + 1):
            for idx_col in range(start_col, final_col + 1):
                if idx_col == col and idx_row in range(irow, frow + 1):
                    continue
                else:
                    if field.itemAtPosition(idx_row, idx_col).widget().get_status() == Status.BUSY:
                        raise RuntimeError("Incorrect set of ships")

    def check_horizontal(self, field, cell):
        row = cell.x
        col = cell.y

        if self.check_busy(row, col) is True:
            return

        if self.right(field, row, col) == Status.BUSY and self.down(field, row, col) == Status.BUSY:
            raise RuntimeError("Incorrect set of ships")

        if self.right(field, row, col) == Status.BUSY and self.down(field, row, col) == Status.FREE:
            init_col = col
            final_col = init_col
            for i in range(num_cells):
                if init_col + i < num_cells and field.itemAtPosition(row, init_col + i).widget().get_status() == Status.BUSY:
                    final_col = init_col + i
                    self.busy_cells.append([row, final_col])
                else:
                    break
            self.check_near_hrz(field, row, init_col, final_col)
            occup_cells = []
            for i in range(init_col, final_col + 1):
                occup_cell = own_player.battlefield.cells[row][i]
                occup_cells.append(occup_cell)

            new_ship = Ship(occup_cells,own_player.battlefield)
            own_player.ships.append(new_ship)
            self.ships.append(new_ship)

        if self.right(field, row, col) == Status.FREE and self.down(field, row, col) == Status.FREE and self.up(field, row, col) == Status.FREE:
            self.busy_cells.append([row, col])
            self.check_near_hrz(field, row, col, col)
            occup_cell = own_player.battlefield.cells[row][col]
            new_ship = Ship([occup_cell],own_player.battlefield)
            own_player.ships.append(new_ship)
            self.ships.append(new_ship)

    def check_vertical(self, field, cell):
        row = cell.x
        col = cell.y

        if self.check_busy(row, col) is True:
            return

        if self.down(field, row, col) == Status.BUSY and self.right(field, row, col) == Status.BUSY:
            raise RuntimeError("Incorrect set of ships")

        if self.down(field, row, col) == Status.BUSY and self.right(field, row, col) == Status.FREE:
            init_row = row
            final_row = init_row
            for i in range(num_cells):
                if init_row + i < num_cells and field.itemAtPosition(init_row + i, col).widget().get_status() == Status.BUSY:
                    final_row = init_row + i
                    self.busy_cells.append([final_row, col])
                else:
                    break
            self.check_near_vrt(field, col, init_row, final_row)
            occup_cells = []
            for i in range(init_row, final_row + 1):
                occup_cell = own_player.battlefield.cells[i][col]
                occup_cells.append(occup_cell)

            new_ship = Ship(occup_cells,own_player.battlefield)
            own_player.ships.append(new_ship)
            self.ships.append(new_ship)

        if self.down(field, row, col) == Status.FREE and self.right(field, row, col) == Status.FREE and self.left(field, row, col) == Status.FREE:
            self.busy_cells.append([row, col])
            self.check_near_vrt(field, row, col, col)
            occup_cell = own_player.battlefield.cells[row][col]
            new_ship = Ship([occup_cell],own_player.battlefield)
            own_player.ships.append(new_ship)
            self.ships.append(new_ship)

    def check_field(self, field):
        try:
            for row in range(num_cells):
                for col in range(num_cells):
                    cell = field.itemAtPosition(row, col).widget().cell
                    if cell.is_busy():
                        self.check_horizontal(field, cell)

            for row in range(num_cells):
                for col in range(num_cells):
                    cell = field.itemAtPosition(row, col).widget().cell
                    if cell.is_busy():
                        self.check_vertical(field, cell)

            return self.check_ships()

        except RuntimeError:
            return False, 'Корабли должны быть расставлены прямо. Рядом ставить корабли запрещено!'


    def reset(self):
        self.busy_cells = []
        self.ships = []

    def get_ships(self):
        return self.ships

    def check_ships(self):
        dim_ship = [4, 3, 2, 1, 0, 0, 0, 0, 0, 0]

        for ship in self.ships:
            index = ship.get_dim() - 1
            if index > 3:
                return False, "Количество пяти и более палубных кораблей должно быть равно 0!"
            dim_ship[index] -= 1

        # if len(self.ships) != 10:
        #     return False, "Количество кораблей должно быть равно 10!"

        if dim_ship[0] != 0:
            return False, "Количество однопалубных кораблей должно быть равно 4! Сейчас их " + str(4 - dim_ship[0])

        if dim_ship[1] != 0:
            return False, "Количество двухпалубных кораблей должно быть равно 3! Сейчас их " + str(3 - dim_ship[1])

        if dim_ship[2] != 0:
            return False, "Количество трехпалубных кораблей должно быть равно 2! Сейчас их " + str(2 - dim_ship[2])

        if dim_ship[3] != 0:
            return False, "Количество четырехпалубных кораблей должно быть равно 1! Сейчас их " + str(1 - dim_ship[3])

        # if sum([abs(ship_left) for ship_left in dim_ship]) > 0:
        #
        #     return False, "Проверьте расстановку! Расставлено больше, чем требуется"
        #
        # if sum([abs(ship_left) for ship_left in dim_ship]) < 0:
        #     return False, "Проверьте расстановку! Расставлено меньше, чем требуется"

        return True, ''


class FieldCell(QtWidgets.QPushButton):
    def __init__(self, cell, parent=None):
        super().__init__(parent)
        self.cell = cell
        self.cell.widget = self
        self.locked = False

        # pix = Qt.QPixmap("sea_cell.jpg")
        # icon = Qt.QIcon(pix)
        # self.setIcon(icon)
        # self.setIconSize(pix.size())
        #self.setStyleSheet('QPushButton { background-color: grey }')
        self.setIcon(Qt.QIcon('./ui/res/sea_cell.jpg'))
        self.setIconSize(Qt.QSize(50, 50))
        self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }') # не спрашивайте. примите как данность
        #self.setIconSize(Qt.QSize(30, 30))

    def mousePressEvent(self, event):
        # print (self.locked)
        # print (game_status)
        if self.locked:
            return
        if self.get_status() == Status.MISS:
            return
        if game_status == GameStatus.PREPARE:
            if self.cell.status == Status.FREE:
                #self.setStyleSheet('QPushButton { background-color: green }')
                self.setIcon(Qt.QIcon('./ui/res/ship_cell.jpg'))
                self.setIconSize(Qt.QSize(50, 50))
                self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность
                self.cell.status = Status.BUSY
            else:
                #self.setStyleSheet('QPushButton { background-color: grey }')
                self.setIcon(Qt.QIcon('./ui/res/sea_cell.jpg'))
                self.setIconSize(Qt.QSize(50, 50))
                self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность
                self.cell.status = Status.FREE
        elif game_status == GameStatus.GAME:
            was_hit = self.try_hit_cell(own_player)
            was_end = player_frame.checkForEndOfGame()
            if (not was_hit and not was_end):
                stop_comp = False
                while (not stop_comp):
                    sleep(COMP_DELAY)
                    can_fire = comp_player.fire_cell()
                    was_end = player_frame.checkForEndOfGame()
                    if (not can_fire or was_end):
                        break

            global num_moves
            num_moves = num_moves + 1

    def get_status(self):
        return self.cell.status

    def shutdown_signal(self):
        self.locked = True

    def try_hit_cell(self,player):
        hit_was = False
        try:
            if self.cell.status == Status.FREE:
                #self.setStyleSheet('QPushButton { background-color: blue }')
                self.setIcon(Qt.QIcon('./ui/res/miss_cell.jpg'))
                self.setIconSize(Qt.QSize(50, 50))
                self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность
                self.cell.status = Status.MISS
                self.repaint()
            elif self.cell.is_busy():
                self.cell.status = Status.HIT
                hit_was = True
                if (not self.cell.ship.is_alive()):
                    self.cell.ship.die()
                else:
                    #self.setStyleSheet('QPushButton { background-color: orange }')
                    self.setIcon(Qt.QIcon('./ui/res/hurt_cell.jpg'))
                    self.setIconSize(Qt.QSize(50, 50))
                    self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность
                    self.repaint()
        except NameError:
            pass
            # print("error in try_hit_cell(): ", NameError)

        return hit_was

    def kill(self):
        try:
            #self.setStyleSheet('QPushButton { background-color: red }')
            self.setIcon(Qt.QIcon('./ui/res/dead_cell.jpg'))
            self.setIconSize(Qt.QSize(50, 50))
            self.cell.widget.setStyleSheet('QPushButton { border-width: -2px; padding: -4px }')  # не спрашивайте. примите как данность

        except NameError:
            pass
            # print("error in kill(): ", NameError)


class PlayerField(QtWidgets.QWidget):
    def __init__(self, parent, child, mode):
        super().__init__(parent)
        self.layout = QtWidgets.QGridLayout(child)
        self.layout.setSpacing(1)
        self.layout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.player = own_player

        for i in range(num_cells):
            for j in range(num_cells):
                btn = FieldCell(self.player.battlefield.cells[i][j])
                self.layout.addWidget(btn, i, j)

    def set_mode(self, mode):
        if mode == FieldProperty.LOCKED:
            for row in range(num_cells):
                for col in range(num_cells):
                   self.layout.itemAtPosition(row, col).widget().shutdown_signal()



class ComputerField(QtWidgets.QWidget):
    def __init__(self, parent, child, mode):
        super().__init__(parent)
        self.layout = QtWidgets.QGridLayout(child)
        self.layout.setSpacing(1)
        self.layout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.player = comp_player

        for i in range(num_cells):
            for j in range(num_cells):
                btn = FieldCell(self.player.battlefield.cells[i][j])
                self.layout.addWidget(btn, i, j)
                btn.setEnabled(False)

    def set_mode(self, mode):
        if mode == FieldProperty.OPEN:
            for row in range(num_cells):
                for col in range(num_cells):
                    self.layout.itemAtPosition(row, col).widget().setEnabled(True)

    def get_item(self, x, y):
        return self.layout.itemAtPosition(x, y).widget().cell



class PlayerFrame(QtWidgets.QWidget):
    def __init__(self, player_names, parent=None):
        global own_player
        own_player = Player(num_cells)
        global comp_player
        comp_player = ComputerPlayer(num_cells, own_player)
        own_player.enemy = comp_player
        comp_player.install_all_ships()
        global num_moves
        num_moves = 0
        global game_status
        game_status = GameStatus.PREPARE

        super().__init__(parent)
        uic.loadUi('./ui/player_frame.ui', self)

        self.player_names = player_names

        global player_frame
        player_frame = self

        self.p_field = None
        self.c_field = None

        self.init_ui()
        self.play_button.clicked.connect(self.start_game)
        self.stop_button.clicked.connect(self.stop_game)
        self.checker = FieldChecker()

    def init_ui(self):
        self.p_field = PlayerField(None, self.player_field, FieldProperty.OPEN)
        self.c_field = ComputerField(None, self.computer_field, FieldProperty.LOCKED)

    def get_player_field(self):
        return self.p_field

    def get_computer_field(self):
        return self.c_field

    def stop_game(self):
        answer = QtWidgets.QMessageBox.question(self, 'Back to main menu?', 'Game progress will be lost. Continue?!',
                                                QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)
        if answer == QtWidgets.QMessageBox.Ok:
            self.close()

    def start_game(self):
        global player_name
        global game_status
        global num_moves

        self.checker.reset()
        is_successful, message = self.checker.check_field(self.player_field.layout())
        if is_successful is False:
            QtWidgets.QMessageBox.critical(self, 'Starting is failed', message)
            return

        stop_edit_name = False
        while (not stop_edit_name):
            player_name, ok = Qt.QInputDialog.getText(self, "What is your name?",
                                            "Player name:", Qt.QLineEdit.Normal)
            if ok is False:
                return

            if (player_name == ''):
                QtWidgets.QMessageBox.critical(self, 'Incorrect name', 'Player name should not be empty!')
            elif (player_name in self.player_names):
                QtWidgets.QMessageBox.critical(self, 'Incorrect name', 'Such player name already exists!')
            else:
                break


        party_hard = QtWidgets.QMessageBox.question(self, 'Select level', 'Party hard? Default = Easy',
                                                    QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

        if party_hard == QtWidgets.QMessageBox.Yes:
            comp_player.set_level(2)
        else:
            comp_player.set_level(1)

        QtWidgets.QMessageBox.information(self, 'Game is started!', 'Good luck! Have fun!')
        self.p_field.set_mode(FieldProperty.LOCKED)
        self.c_field.set_mode(FieldProperty.OPEN)
        game_status = GameStatus.GAME
        num_moves = 0

    def checkForEndOfGame(self):
        if (own_player.is_defeated()):
            QtWidgets.QMessageBox.critical(self, 'YOU DIED!', 'Computer wins!')
            self.endTheGame()
            return True
        elif (comp_player.is_defeated()):
            global num_moves
            QtWidgets.QMessageBox.information(self, 'YOU WON!', 'Computer died!\n' \
                                                                'You needed %d moves for that!' % num_moves)
            self.addToRecords()
            self.endTheGame()
            return True

        return False

    def addToRecords(self):
        global player_name
        global num_moves

        recs = []
        with open('records.rec', 'r+', encoding='utf-8') as records:
            for line in records:
                recs.append(line.split(';'))

        recs.append([player_name, num_moves])

        sortedRecs = sorted(recs, key=lambda record: int(record[1]))

        recordsFile = open('records.rec', 'w+')
        for line in sortedRecs:
            recordsFile.write('%s;%d\n' % (line[0], int(line[1])))
        recordsFile.close()

    def endTheGame(self):
        global game_status
        game_status = GameStatus.END
        self.p_field.set_mode(FieldProperty.LOCKED)
        self.c_field.set_mode(FieldProperty.LOCKED)
        self.close()


# if __name__ == '__main__':
#     app = QtWidgets.QApplication(sys.argv)
#     player_frame = PlayerFrame()
#     player_frame.show()
#     app.exec_()